const express = require('express')
const bodyParser = require('body-parser')
const routes = require('./routes/routes.js')
const app = express()
const router = require('./router.js')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true,
  limit: '5mb'
}));
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS')
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, ContentC')
  res.header('Access-Control-Request-Method', 'POST, GET, OPTIONS, PUT, DELETE, HEAD')
  next()
})
routes(app)

const server = app.listen(3000, function () {
  console.log('app running on port.', server.address().port)
})

// const express = require('express');
// const multer  = require('multer');
// const Sharp = require('sharp');

// let origin_path = 'storage/origin/';
// let optimized_path = 'storage/optimized/';

// const app = express();

// const storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//         cb(null, origin_path)
//     },
//     filename: function (req, file, cb) {
//         let extension = file.originalname.split(".");
//         cb(null, Date.now() + '.' + extension[extension.length - 1])
//     }
// });

// let upload = multer({storage: storage});

// app.post('/upload', upload.single('file'), function (req, res) {
//     let img_dest = optimized_path + req.file.filename;
//     Sharp(origin_path + req.file.filename)
//         .jpeg({quality:50})
//         .webp({quality:50})
//         .png({quality:50})
//         .toFile(img_dest, function (err) {
//             res.send(err);
//         });
// });

// app.get('/', function (req, res) {
//     res.send('Hello World');
// });

// let server = app.listen(8081, function () {
//     let host = 'localhost';
//     let port = server.address().port;
//     console.log("App listening at http://%s:%s", host, port);
// });
