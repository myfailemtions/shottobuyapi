const multer = require('multer');
const path = require('path')

let origin_path = 'uploads';

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, origin_path)
    },
    filename: function (req, file, cb) {
        let extension = file.originalname.split(".");
        cb(null, 'file' + '.' + extension[extension.length - 1])
    }
});

let upload = multer({ storage });
module.exports = upload;