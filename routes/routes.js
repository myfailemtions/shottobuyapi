const storage = require('./fileUploader')

async function recognizeImage(image) {
  // Imports the Google Cloud client library
  const vision = require('@google-cloud/vision')

  // Creates a client
  const client = new vision.ImageAnnotatorClient()

  // Performs label detection on the image file
  const [result] = await client.labelDetection(image)
  const labels = result.labelAnnotations
  console.log('Labels:')
  labels.forEach(label => console.log(label.description))
  return labels
}

const appRouter = app => {
  app.post('/image', storage.single('file'), controller)
  app.get('/translate', translationController)
}

const translationController = async ({ query: { text, target } }, res) => {
  // Imports the Google Cloud client library
  const { Translate } = require('@google-cloud/translate');

  // Creates a client
  const translate = new Translate();

  let [translations] = await translate.translate(text, target);
  translations = Array.isArray(translations) ? translations : [translations];
  console.log('Translations:');
  translations.forEach((translation, i) => {
    console.log(`${text[i]} => (${target}) ${translation}`);
  });
  res.status(200).send({
    searchWork: translations[0]
  })
}

const controller = async ({ body }, res) => {
  const result = await recognizeImage('./uploads/file.jpeg')
  return res.status(200).send({
    success: true,
    message: 'Hi',
    result
  })
}

module.exports = appRouter
